<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $datas = Todo::orderBy('id', 'DESC')->paginate(10);

        return response()->json([
            'status' => 'success',
            'result' => $datas
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'result' => $validator->errors(),
            ]);
        }

        $model = new Todo;
        $model->title = $request->title;
        $model->description = $request->description;

        $model->save();

        return response()->json([
            'status' => 'success',
            'result' => $model,
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $decryptId = Crypt::decryptString($id);
            
            $model = Todo::find($decryptId);

            return response()->json([
                'status' => 'success',
                'result' => $model,
            ]);

        } catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
            return response()->json([
                'status' => 'error',
                'result' => $e,
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'result' => $validator->errors(),
            ]);
        }

        try {
            $decryptId = Crypt::decryptString($id);
            
            $model = Todo::find($decryptId);
            $model->title = $request->title;
            $model->description = $request->description;
    
            $model->save();

            return response()->json([
                'status' => 'success',
                'result' => $model,
            ]);

        } catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
            return response()->json([
                'status' => 'error',
                'result' => $e,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $decryptId = Crypt::decryptString($id);
            
            $model = Todo::find($decryptId);
            $model->delete();

            return response()->json([
                'status' => 'success',
                'result' => 'Data berhasil dihapus.',
            ]);

        } catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
            return response()->json([
                'status' => 'error',
                'result' => $e,
            ]);
        }
    }
}
